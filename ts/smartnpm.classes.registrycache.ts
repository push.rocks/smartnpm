import { NpmRegistry } from './smartnpm.classes.npmregistry.js';
import * as plugins from './smartnpm.plugins.js';


export interface ICacheDescriptor {
  registryUrl: string;
  packageName: string;
  filePath: string;
  distTag?: string;
  version?: string;
}

export class RegistryCache {
  npmregistryRef: NpmRegistry;
  public levelCache: plugins.levelcache.LevelCache;

  constructor(npmRegistryRefArg: NpmRegistry) {
    this.npmregistryRef = npmRegistryRefArg;
    this.levelCache = new plugins.levelcache.LevelCache({
      cacheId: encodeURIComponent(this.npmregistryRef.options.npmRegistryUrl),
      
    });
  }

  public async getCachedFile (cacheDescriptorArg: ICacheDescriptor): Promise<plugins.smartfile.Smartfile> {
    const cacheEntry = await this.levelCache.retrieveCacheEntryByKey(this.getCacheDescriptorAsString(cacheDescriptorArg));
    if (cacheEntry) {
      return plugins.smartfile.Smartfile.fromFoldedJson(cacheEntry.contents.toString());
    }
    return null;
  }

  public async cacheSmartFile (cacheDescriptorArg: ICacheDescriptor, smartfileArg: plugins.smartfile.Smartfile) {
    if (smartfileArg && cacheDescriptorArg.version) {
      await this.levelCache.storeCacheEntryByKey(this.getCacheDescriptorAsString(cacheDescriptorArg), new plugins.levelcache.CacheEntry({
        contents: Buffer.from(smartfileArg.foldToJson()),
        ttl: plugins.smarttime.getMilliSecondsFromUnits({hours: 1})
      }));
    } else {
      await this.levelCache.storeCacheEntryByKey(this.getCacheDescriptorAsString(cacheDescriptorArg), new plugins.levelcache.CacheEntry({
        contents: Buffer.from(smartfileArg.foldToJson()),
        ttl: plugins.smarttime.getMilliSecondsFromUnits({minutes: 1})
      }));
    }
  }

  public getCacheDescriptorAsString(cacheDescriptorArg?: ICacheDescriptor) {
    return `${cacheDescriptorArg.registryUrl}//+//${cacheDescriptorArg.packageName}//+//${cacheDescriptorArg.filePath}//+//${cacheDescriptorArg.distTag || cacheDescriptorArg.version}`;
  }
}